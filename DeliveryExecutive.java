import java.util.*;

class DeliveryExecutive{

    int id;
	double lat; 
    double lon;
    Date time;
    int locationId;
    
    public DeliveryExecutive(int id, double lat, double lon, Date time, int locationId) {
        this.id   = id;
    		this.lat  = lat;
        this.lon  = lon;
        this.time = time;
        this.locationId = locationId;
    }
    
    public int getId()
    {
    		return this.id;
    }
    
    public double getLat()
    {
    		return this.lat;
    }
    
    public double getLon()
    {
    		return this.lon;
    }
    
    public Date getTime() 
    {
    		return this.time;
    }
    
    public int getLocationId() 
    {
    		return this.locationId;
    }
}
