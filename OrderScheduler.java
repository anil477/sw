public class OrderScheduler {

	public OrderScheduler()
	{
	}
	
	public static void main(String args[]) {
		
		/* This class is responsible for fetching the order from all the locations
		 * and fetching the a DE for that order
		 * There will a number of threads which check each LocationManager in parallel 
		 * It fetches the order which should be assigned from each location 
		 * Then it tries to pickup an appropriate DE  
		 */
		
		OrderFetcher oderFetcher = new OrderFetcher();
		oderFetcher.incomingOrder(1, 12.9353083, 77.63094079999996);
		oderFetcher.incomingOrder(2, 12.9603447, 77.64172110000004);
		oderFetcher.incomingOrder(3, 13.0091991, 77.71192070000006);
		
		OrderLocationManager1 loc = OrderLocationManager1.getInstance();
		Order newOrder = loc.getNextOrder();
		
		DEFetcher deFetcher = new DEFetcher();
		deFetcher.incomingDE(1, 12.899623, 77.48269759999994);
		deFetcher.incomingDE(2, 13.1004849, 77.5940134);
		deFetcher.incomingDE(3, 13.1986348, 77.70659279999995);
		
		DeliveryExecutiveManagement de = DeliveryExecutiveManagement.getInstance();
		OrderDeliveryDetails orderDetails = de.selectExecutive(newOrder);
	}
}
