class OrderManagement{
    
    private static OrderManagement instance;
    
    public static OrderManagement getInstance(){
        if(instance == null){
            instance = new OrderManagement();
        }
        return instance;
    }
    
    private OrderManagement(){
   	 
    }
    
    public static void main(String args[])
    { 
    }
    
    
    public void addWaitingOrder(Order order)
    {
        // we instantiate the class based on the locationId specified in the order  
    		// here we are add to hard-coded location manager class - LocationManager1  
    	    OrderLocationManager1 loc = OrderLocationManager1.getInstance();
    		loc.addOrder(order);
    }
}
