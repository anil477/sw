We assume that our entire service area is divided into a number of location. The entire city can be a location or it can be divided into multiple location. Like Kormangla can be one location, HSR can be another location. Each such location is identified by a locationId.

Haversine: find the Haversine distance
	- double distance(double startLat, double startLong, double endLat, double endLong)

Order: Order related details
    - properties: id, lat, lon, priority, time, locationId
    - method:  
        getId()
        getPriority()
        getLat()
        getLon()
        getTime ()
        getLocationId()

OrderFetcher: Handles all the incoming order
	- method:
		int getLocationId(double lat, double lon) - it takes the latitude and longitude and returns the locationId.
		main() - it creates an order based on the order latitude, longitude, locationid, order creation time and some priority if needed. The order is added via OrderManagement class.

OrderManagement: Handles the adding of newly created order. 
    - method: 
		void addWaitingOrder(Order order) - Each order is mapped to a locationId. Based on this locationId the class instantiates a OrderLocationManager which maiantians all the order of that particular location.

OrderLocationManagerInterface: All OrderLocationManager class implement this interface
	- method:
		void addOrder(Order order)
		Order getNextOrder()

OrderLocationManager: implements OrderLocationManagerInterface 
	- method:
		void addOrder(Order order)
		Order getNextOrder()
		There will be several such Order Location Manager specific to locationId like OrderLocationManager1 for locationId = 1. Each of these location class will be customized as per the special requirement of the area. Like we can easliy cutomize that the orders of this location will be processed faster than the other location or prioritize delayed order. Each of these class will save the order in any way it seems fit. It can have it's own prioritizing algo.	

DeliveryExecutive: DeliveryExecutive related details
    - properties: id, lat, lon, priority, time, time, locationId
    - method:  
        getId()
        getPriority()
        getLat()
        getLon()
        getTime ()
        getLocationId()

DEFetcher : Handles all the incoming idle delivery executive details
	- method:
		int getLocationId(double lat, double lon) - it takes the latitude and longitude and returns the locationId.
		main() - it creates an DeliveryExecutive based on the latitude, longitude, locationid and adds via DeliveryExecutiveManagement. 

DeliveryExecutiveManagement: 		
	- method:
		addExecutive(DeliveryExecutive de) - Handles the part of adding DE with locationId
		selectExecutive(Order order) - Based on the order locationId and latitude and longitude of order it will select a DE.

DELocationManagerInterface:
	- method:
		addExecutive(DeliveryExecutive de)
		DeliveryExecutive selectExecutive(double resLat, double resLon)

DELocationManager: implements DELocationManagerInterface
	- method:
	    addExecutive(DeliveryExecutive de)
	    DeliveryExecutive selectExecutive(double resLat, double resLon)
	
	There will be several such Order Location Manager specific to locationId like DELocationManager1 for locationId = 1. Each of these location class will be customized as per the special requirement of the area. 
	Like we could give priority of highly rated DE or equally distribute orders among DE. Each of these class will save the DE details in any way it seems fit. It can have it's own prioritizing algo.    

OrderScheduler - 
    This is the main class which does order selection. It will have multiple threads each for every location we serve. It will pickup the order from each location and then call DELocationManager to select as DE for that order. Here we can give priority to any particular location. 
    Each location has it's own implementation of selecting the order to be delivered and selecting DE. This will provide us with flexibility to change the system anytime.

OrderDeliveryDetails - the orders details
