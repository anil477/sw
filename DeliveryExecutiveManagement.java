import java.util.*;

class DeliveryExecutiveManagement {

    private static DeliveryExecutiveManagement instance;
    
    public static DeliveryExecutiveManagement getInstance(){
        if(instance == null){
            instance = new DeliveryExecutiveManagement();
        }
        return instance;
    }
    
    private DeliveryExecutiveManagement(){
    }
    
    public static void main(String args[])
    { 
    }
    
    public void addExecutive(DeliveryExecutive de)
    {
    		// we instantiate the class based on the locationId specified in the order  
		// here we are add to hard-coded location manager class - DELocationManager1  
	    DELocationManager1 dem = DELocationManager1.getInstance();
		dem.addExecutive(de);               
    }
    
    public OrderDeliveryDetails selectExecutive(Order order)
    {
    		// call the appropriate DELocationManager based on order
    		// using DELocationManager1 here 	
    		DELocationManager1 dem = DELocationManager1.getInstance();
    		DeliveryExecutive selectedExecutive = dem.selectExecutive(order.getLat(), order.getLon());
    		OrderDeliveryDetails od = new OrderDeliveryDetails(order.id, selectedExecutive.id);
        return od;
    }
}