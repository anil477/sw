import java.util.*;

public class OrderFetcher {

	public OrderFetcher()
	{
		
	}
	
	public static void main(String args[]) {
	}
	
	public void incomingOrder(int id, double lat, double lon)
	{
		Calendar cal = Calendar.getInstance();
		int locationId = getLocationId(lat, lon);
		Order order = new Order(id,lat,lon,cal.getTime(),0,locationId);
		OrderManagement manageOrder = OrderManagement.getInstance();
        manageOrder.addWaitingOrder(order);
	}
	
	private static int getLocationId(double lat, double lon)
	{
		// we assume that we have divided the city into 4 major location - north, east, west, south
		// when a order comes in based on the lat, log we map it to one of these locations
		// this method find the location based on this lat, log
		// this method will call some class `LocationMapper` and return that value  
		return 1;
	}
}
