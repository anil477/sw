import java.util.*;

class DELocationManager1 {
    
    static ArrayList<DeliveryExecutive> deList;
    
    private static DELocationManager1 instance;
    
    public static DELocationManager1 getInstance(){
        if(instance == null){
            instance = new DELocationManager1();
            deList = new ArrayList<DeliveryExecutive>();
        }
        return instance;
    }
    
    public static void main(String args[])
    { 
        
    }
    
    public void addExecutive(DeliveryExecutive de)
    {
    		deList.add(de);               
    }
    
    public DeliveryExecutive selectExecutive(double resLat, double resLon)
    {
        PriorityQueue<DeliveryExecutive> queue = new PriorityQueue<>(3, new Comparator<DeliveryExecutive>() {
        @Override
        public int compare(DeliveryExecutive de1, DeliveryExecutive de2) {
		        Date d1 = de1.getTime();
		        Date d2 = de2.getTime();
		        return d1.compareTo(d2);
	    		}
        });
        
        // find the top 3 closest delivery executive
        Iterator<DeliveryExecutive> ite = deList.iterator();
		while (ite.hasNext()) {
			DeliveryExecutive currentList = ite.next();
			DeliveryExecutive heapTop = queue.peek();
			if(queue.size() <=3) {
			    queue.add(currentList);
			} else {
			    double disList = Haversine.distance(currentList.lat, currentList.lon, resLat, resLon);        
                double disHeap = Haversine.distance(heapTop.lat, heapTop.lon,resLat, resLon); 
                System.out.println(" list: " + disList + " disHeap: " + disHeap);
                if(disHeap > disList)  {
                    queue.poll();
                    queue.add(currentList);
                }
			}
		}
		
		// now we have 3 closet executive. find the one with max waiting time
		Iterator<DeliveryExecutive> it = queue.iterator();
		DeliveryExecutive selected = it.next();
		Date d = selected.getTime(); 
		
        while (it.hasNext()) {
            DeliveryExecutive de = it.next();
            System.out.println(de.getId());
            Date d2 = de.getTime();
            if(d.after(d2)) {
                selected =  de;
                d = de.getTime();
            }
        }
        deList.remove(selected);
        // System.out.println(deList.toString());
        return selected;
    }
}