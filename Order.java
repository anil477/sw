import java.util.Date;

class Order
{
	int id;
	Date time;
	double lat; 
    double lon;
    int priority;
    int locationId;
    
    public Order(int id, double lat, double lon, Date time, int priority, int locationId) {
        this.id   = id;
    		this.lat  = lat;
        this.lon  = lon;
        this.time = time;
        this.priority   = priority;
        this.locationId = locationId;
    }
    
    
    public int getId()
    {
    		return this.id;
    }
    
    public int getPriority()
    {
    		return this.priority;
    }
    
    public double getLat()
    {
    		return this.lat;
    }
    
    public double getLon()
    {
    		return this.lon;
    }
    
    public Date getTime() 
    {
    		return this.time;
    }
    
    public int getLocationId() 
    {
    		return this.locationId;
    }
}

