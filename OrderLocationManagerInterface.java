
public interface OrderLocationManagerInterface {
	void addOrder(Order order);
	Order getNextOrder();
}
