import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Date;

class OrderLocationManager1{
    
    static PriorityQueue<Order> orderQueue;
    private static OrderLocationManager1 instance;
    
    public static OrderLocationManager1 getInstance(){
        if(instance == null){
            instance = new OrderLocationManager1();
            orderQueue = new PriorityQueue<>(100, new Comparator<Order>() {
    	        @Override
    	        public int compare(Order o1, Order o2) {
    	        	    int p1 = o1.getPriority();
    	        	    int p2 = o2.getPriority();
    	            
    	            if(p1 == p2) 
    	            {
    	               Date d1 = o1.getTime();
    	               Date d2 = o2.getTime();
    	               return d1.compareTo(d2);
    	            }
    	            else 
    	            {
    	            	    return p1 > p2 ? -1 : p1 == p2 ? 0 : 1;
    	            }
    	        }
    	       });
        }
        return instance;
    }
    
    private OrderLocationManager1(){
    	 
    }
   
    public static void main(String args[])
    { 
  
    }
    
    public void addOrder(Order order)
    {
    		// add the order to the queue. This class will define it's own priority ordering
    		orderQueue.add(order); 
    }
    
    /*
     * Here we will have logic to change current order details.
     * Suppose for this locationId we need the sorting should be based on priority 
     * At the same time we don't want to starve low priority order  
     * This method will priority of orders based on time or arrival
     * This will increase the priority of low priority task as time elapses   
     */
    private void changeOrder()
    {
    		
    }
    
    public Order getNextOrder()
    {
    		Order orderToAssign = orderQueue.poll();
    		this.changeOrder();
    		return orderToAssign;
    }
}
