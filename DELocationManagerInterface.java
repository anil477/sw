
public interface DELocationManagerInterface {
	
	public void addExecutive(DeliveryExecutive de);
	public DeliveryExecutive selectExecutive(double resLat, double resLon);
}
